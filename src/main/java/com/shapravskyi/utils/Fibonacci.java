package com.shapravskyi.utils;

/**
 * <h1>Fibonacci</h1>
 * This is an abstract class that contains functions
 * for calculating and printing the Fibonacci sequence
 *
 * @author Andrii Shapravskyi
 * @version 1.0
 * @since 2018-11-11
 */
public abstract class Fibonacci {
    /**
     * This method is used to display the Fibonacci sequence on the console.
     * @param interval Length (number of elements) of the sequence.
     */
    public static void printSequence(int interval) {
        for(int i = 1; i < interval; i++) {
            System.out.print(calculate(i) + " ");
        }
    }

    /**
     * A method for calculating a numbers from a sequence.
     * @param index Address of a single number in the Fibonacci sequence.
     * @return int Single number from the Fibonacci sequence.
     */
    public static int calculate(int index) {
        if (index <= 0) {
            return 0;
        } else if (index == 1) {
            return 1;
        } else if (index == 2) {
            return 1;
        } else {
            return calculate(index - 1) + calculate(index - 2);
        }
    }
}
